<?

require_once( $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');


function AgentBirthday(){

	$data_file = $_SERVER['DOCUMENT_ROOT'] . '/upload/users.xlsx';

	if( is_file( $data_file ) === false ){

		//echo 'Файл с данными "' . $data_file . '" не найден.' . PHP_EOL;
		//exit;

		return 'AgentBirthday()';

	}

	// region Прочитать данные из Excel'евского файла.

	$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

	$spreadsheet = $reader->load($data_file);

	$sheet = $spreadsheet->getActiveSheet();

	$users = [];

	$first = false;

	foreach( $sheet->getRowIterator() as $row ){

		if( $first === false ){
			$first = true;
			continue;
		}


		$cellIterator = $row->getCellIterator();

		$cellIterator->setIterateOnlyExistingCells(false);

		$cells = [];


		foreach( $cellIterator as $cell ){

			$cells[] = $cell->getValue();

		}

		if( filter_var( $cells[1], FILTER_VALIDATE_EMAIL ) !== false ) {

			$date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($cells[2]);

			// Отобрать только тех пользователей, у которых сегодня день рождения.
			if( $date->format('d.m') === date('d.m') ){

				$cells[2] = $date->format('d.m.Y');

				$users[] = $cells;

			}

		}

	}


	//print_r($users);
	//exit;

	// endregion


	// region Получить список существующих пользователей.

	// Можно было бы сделать пошаговый режим, но для абстрактной тестовой задачи, полагаю достаточно штучного перебора записей!?
	// Также можно было бы использовать поле "дата рождения" в самом Битриксе, тогда не потребовалась работа с Excel файлом.

	$db = \Bitrix\Main\Application::getConnection();

	$sqlHelper = $db->getSqlHelper();

	$existed_users = [];

	foreach( $users as $user ){

		$sql = 'SELECT ID, EMAIL FROM b_user WHERE EMAIL = "' . $sqlHelper->forSql( $user[1] ) . '"';

		$record = $db->query( $sql )->fetch();

		if( is_array( $record ) === true ){

			$existed_users[] = $record;

		}

	}

	// endregion



	//print_r($existed_users);
	//exit;


	// region Отправить сообщение.

	if( count( $existed_users ) > 0 ) {

		\CEvent::SendImmediate(
			'BIRTHDAY_USERS',
			's1', //SITE_ID,
			[
				'USERS' => implode(PHP_EOL, $existed_users)
			]
		);

	}


	// endregion



	return 'AgentBirthday()';

}


?>